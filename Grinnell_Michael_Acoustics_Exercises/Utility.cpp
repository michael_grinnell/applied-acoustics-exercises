#include "Utility.hpp"
#include <iostream>

bool validChoice(char* input, unsigned short max)
{

	if (input[strspn(input, "0123456789")] == 0)
	{
		if (strtol(input, NULL, 10) <= max)
			return true;
	}

	return false;
}

void print(std::string message, bool newLine)
{
	std::cout << message;

	if (newLine)
		std::cout << std::endl;
}