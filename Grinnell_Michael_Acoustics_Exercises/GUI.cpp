﻿#include "GUI.hpp"

#include <stdio.h>
#include <iostream>
#include "Utility.hpp"

GUI::GUI(){}

void GUI::PrintMain()
{
	// Forgot I was using C++ instead of C, which is why im using printf, I might not get around to changing it to cout
	printf("\n________________________________________________________________________________\n\n");
	printf("\n\tMICHAEL GRINNELL\n");
	printf("\tApplied Acoustics: \n");
	printf("\tAcoustics exercises\n");

	printf("\n\tPlease Enter the number of the Question to run\n");

	print("\n\tLab 1 Questions from 1 - 15, Lab 2 Questions From 17 - 23\n");

	printf("\n\t(0) EXIT\n");
	printf("\n________________________________________________________________________________\n\n");

	printf("\nChoice: ");
}

void GUI::PrintQuestion(unsigned short question)
{
	printf("\n________________________________________________________________________________\n\n");
	printf("\tQuestion %d\n", question);
	printf("\n________________________________________________________________________________\n\n");
	switch (question)
	{

	case 0:
		printf("\n\t\t\t\tGOODBYE!\n\n");
		wave();
		break;

	case 1:
		printf("\tWork out the period, and wavelength for each of the following notes\n");
		printf("\tAssuming a Temprature of 21C\n");
		break;

	case 2:
		printf("\tWork out the frequency of the following notes, based on their wavelengths\n");
		printf("\tAssuming a Temprature of 21C\n");
		break;

	case 3:
		printf("\t Using the formula given below, work out the speed of sound in the following materials:\n");
		break;

	case 4:
		std::cout << " How long does it take sound to travel to the back of an auditorium\n if the air "
			"temperature is 30C and the stage is 35m away ?" << std::endl;
		break;

	case 5:
		std::cout << " It's a terrible afternoon and you see storm clouds moving in on the city.\n"
			" All of a sudden you see a flash of lightning, then 3 seconds later you hear a rumble of thunder.\n"
			" If the temperature outside is 16°C how far away is the thunder cloud ?\n" << std::endl;
		break;

	case 6:
		std::cout << " What is the difference in sound velocity between sounds travelling at sea level, (20C),\n"
			" and sound travelling at the air at the top of Mt.Everest (-36°C).\n" << std::endl;
		break;

	case 7:
		std::cout << " What is the frequency of a second hand on a clock?.\n" << std::endl;
		break;

	case 8:
		std::cout << " What is the frequency of the earth's rotation?" << std::endl;
		break;

	case 9:
		std::cout << " Sum the following waveforms and plot the result into the empty graph " << std::endl;
		break;

	case 10:
		std::cout << " If a violin string vibrates at 1720Hz what is the length of one wavelength?" << std::endl;
		break;

	case 11:
		std::cout << " If pipe on a pipe organ is built to have a wavelength of 4m's\n what is the frequency of the pipe ?" << std::endl;
		break;

	case 12:
		std::cout << " Calculate the SWL for a sound source which radiates a total of 0.5 Watt, 1 Watts and 0.25 Watts. " << std::endl;
		break;

	case 13:
		std::cout << " What is the SPL of a wave with RMS pressure amplitude of 1.5 Pa's?" << std::endl;
		break;

	case 14:
		std::cout << " Using the equation below, and using a reference level of 10-12 W/m2 \n work"
			" out the total SIL of a loudspeaker radiating 100mWs\n at a distance of/n 1m, 2m,"
			" and 4m from the speaker ?" << std::endl;
		break;

	case 15:
		std::cout << " A loudspeaker radiates 100mW’s at a distance of 2m’s when it is mounted "
			"on 1, 2, and 3 boundaries respectively.\n What is the SIL of each for each case ? " << std::endl;
		break;

	case 17:
		print(" The average distance a sound travels between two successive reflections it is called the mean\n"
			" free path and is worked out using the following equation : 4V/S\n");

		print(" where V is the volume of the room and S is the total surface area of the room.\n"
			" Using the above formula work out the mean free path of sound in a room measuring 2m x 3m x 5m\n");
		break;

	case 18:
		print(" Now work out the average time it takes between successive reflections");
		break;

	case 19:
		print("Using the formula above and the chart given below, work out the RT60 times for the following rooms at 250Hz:");
		break;

	case 20:
		print(" Using the charts and formula above, work out the difference in reverberation time for a piccolo\n"
			" playing a note at 4kHz and a French horn playing a note at 125kHz in the following room : ");
		break;

	case 21:
		print(" Work out F0 for a 1/4inch plywood flat panel absorber measuring 1m x 1m with depth of 10cm");
		break;

	case 22:
		print(" Work out F0 for a 1/4inch plywood flat panel absorber measuring 1m x 1m with depth of 20cm");
		break;

	case 23:
		print(" Work out F0 for a 1/2inch plywood flat panel absorber measuring 1m x 2m with depth of 10cm");
		break;

	case 24:
		print(" You are working in a room with noticeable problems around 170Hz. You need to construct so\n"
			" panel absorbers.Choosing any thickness plywood you wish, work out the dimensions and cavity\n"
			" depth needed in order to absorb energy at that frequency");
		break;

	default:
		break;
	}
	printf("\n________________________________________________________________________________\n");
}

void GUI::PrintConclusion()
{
	printf("\n\tFrom these results we can conclude that \n");
	printf("\tWave period and Wave frequency are the multiplicative inverse of one another and,\n");
	printf("\tFrequency and wavelength are inversely proportional to one another\n ");
	printf("\n________________________________________________________________________________\n");
}

//https://www.geeksforgeeks.org/program-to-print-sine-wave-pattern/
void GUI::wave() {

	printf("\n________________________________________________________________________________\n\n");

	int wave_height = 5, wave_length = 10;

	int is = 1, os = 2;

	// for loop for height of wave 
	for (int i = 1; i <= wave_height; i++) {

		// for loop for wave length 
		for (int j = 1; j <= wave_length; j++) {

			// intermediate spaces 
			for (int k = 1; k <= os; k++) {
				printf(" ");
			}

			// put any symbol 
			printf("0");

			for (int k = 1; k <= is; k++)
				printf(" ");

			// put any symbol 
			printf("0");

			for (int k = 1; k <= os; k++)
				printf(" ");

			printf(" ");
		}

		// set a value of os to 1 if i+1 is not  
		// equal to wave height or 0 otherwise 
		os = (i + 1 != wave_height);

		// set value of is to 3 if i+1 is not equal  
		// to wave height or 5 oterwise 
		is = (i + 1 != wave_height) ? 3 : 5;

		printf("\n");
	}
}
