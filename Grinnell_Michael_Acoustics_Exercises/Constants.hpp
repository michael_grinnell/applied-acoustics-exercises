#pragma once
#include <math.h>

// Speed of sound in air at 0C
const float SPEED_OF_SOUND = 331.5f;
const double PI = 3.141592653589793238463;

// Increase in Speed per degree
// https://hypertextbook.com/facts/2000/CheukWong.shtml
const float TEMP_INCREASE = 0.60f;

//Young's modulus
const double Y_MOD_STEEL = 2.1 * pow(10,11);
const double Y_MOD_ALUMINIUM = 6.9 * pow(10, 10);
const double Y_MOD_LEAD = 1.7 * pow(10, 10);
const double Y_MOD_GLASS = 6 * pow(10, 10);
const double Y_MOD_CONCRETE = 3 * pow(10, 10);
const double Y_MOD_WATER = 2.3 * pow(10, 9);
const double Y_MOD_BEACH_WOOD_X = 1.4 * pow(10, 10); // along the grain
const double Y_MOD_BEACH_WOOD_Y = 8.8 * pow(10, 8); // across the grain

//Denstiy
const unsigned int DENSITY_STEEL = 7800;
const unsigned int DENSITY_ALUMINIUM = 2720;
const unsigned int DENSITY_LEAD = 11400;
const unsigned int DENSITY_GLASS = 2400;
const unsigned int DENSITY_CONCRETE = 2400;
const unsigned int DENSITY_WATER = 1000;
const unsigned int DENSITY_BEACH_WOOD = 680;

//LAB 2 Constants

//Material Coefficient frequencies
enum MC {
	_125HZ,
	_250HZ,
	_500HZ,
	_1KHZ,
	_2KHZ,
	_4KHZ
};


//Material Coefficients			125HZ, 250HZ, 500HZ, 1KHZ,  2KHZ,  4KHZ
const float BRICK_NATURAL[] = { 0.03f, 0.03f, 0.03f, 0.04f, 0.05f, 0.07f };
const float BRICK_PAINTED[] = { 0.01f, 0.01f, 0.02f, 0.02f, 0.02f, 0.03f };
const float PLASTERBOARD[]  = { 0.15f, 0.11f, 0.04f, 0.04f, 0.07f, 0.08f };
const float GLAZED_TILE[]   = { 0.01f, 0.01f, 0.01f, 0.01f, 0.02f, 0.02f };
const float WOOD_FLOORING[] = { 0.15f, 0.11f, 0.1f,  0.07f, 0.06f, 0.07f };
const float PLASTER[]		= { 0.01f, 0.02f, 0.02f, 0.03f, 0.04f, 0.05f };
const float CONCRETE_BLOCK[]= { 0.36f, 0.44f, 0.31f, 0.29f, 0.39f, 0.25f };
const float CONCRETE[]		= { 0.01f, 0.02f, 0.04f, 0.06f, 0.08f, 0.1f };


//Surface Densitys kg/m2
const float PLY_1_4_INCH = 3.47f;
const float PLY_1_2_INCH = 6.93f;