//disable compiler warning for scanf
#define _CRT_SECURE_NO_WARNINGS

#include <iomanip>  

#include "Application.hpp"
#include "GUI.hpp"
#include "Utility.hpp"
#include "Math.hpp"
#include "Materials.hpp"
#include "Constants.hpp"


Application::Application(){}

void Application::start()
{
	GUI gui;
	gui.wave();
	run();
}

void Application::run()
{
	GUI gui;
	gui.PrintMain();

	char input[1];
	scanf("%s", input);

	if (!validChoice(input, 24))
	{
		print("\nPlease Enter A valid Choice\n");
		run();
	}

	unsigned short choice = strtol(input, NULL, 10);

	gui.PrintQuestion(choice);

	switch (choice)
	{
	case 0:
		exit(0);
		break;

	case 1:
		Q1();
		gui.PrintConclusion();
		break;

	case 2:
		Q2();
		gui.PrintConclusion();
		break;

	case 3:
		Q3();
		break;

	case 4:
		Q4();
		break;

	case 5:
		Q5();
		break;

	case 6:
		Q6();
		break;

	case 7:
		Q7();
		break;

	case 8:
		Q8();
		break;

	case 9:
		Q9();
		break;

	case 10:
		Q10();
		break;

	case 11:
		Q11();
		break;

	case 12:
		Q12();
		break;

	case 13:
		Q13();
		break;

	case 14:
		Q14();
		break;

	case 15:
		Q15();
		break;

	case 17:
		Q17();
		break;

	case 18:
		Q18();
		break;

	case 19:
		Q19();
		break;

	case 20:
		Q20();
		break;

	case 21:
		Q21();
		break;

	case 22:
		Q22();
		break;

	case 23:
		Q23();
		break;

	case 24:
		Q24();
		break;

	}

	std::cout << "\n\n" << std::endl;
	system("pause");
	run();
}

#pragma region Lab 1 Questions
void Application::Q1()
{

	std::cout << "\n Peroiod is found by 1(Second)/frequency and is the time taken to complete one cycle.\n"
		<< " Wavelength is found by speed/frequency and is the distance corresponding points of two consecutive waves\n" << std::endl;

	float frequencys[] = { 100.f, 200.f, 250.f, 275.f }, temperature = 21.f;

	for (const float& frequency : frequencys)
		printf("\n %.0fHZ - Period: %.6f Seconds, WaveLength: %.2f meters\n", frequency, CalculatePeriod(frequency), CalculateWaveLength(frequency, temperature));
}

void Application::Q2()
{
	//Divide the velocity of the wave, V, by the wavelength converted into meters

	std::cout << "\n Frequency is found by speed/wavelength and is the number cycles of a wave that pass in one second\n" << std::endl;

	float waveLengths[] = { 344.f, 172.f, 0.25f, 0.6f, 1.f }, temperature = 21.f;

	for (const float& waveLength : waveLengths)
		printf("\n %.2fm - Frequency: %.2f HZ\n", waveLength, CalculateFrequency(waveLength, temperature));
}

void Application::Q3()
{
	std::cout << "\n The speed of sound in a solid is calculated using Young's modulus\n"
		<< " This measures the stiffness of a material, The speed is calculated by\n"
		<< " Taking the square root of Youngs Mod / Density \n" << std::endl;
	Materials materialList;
	materialList.Print();
}

void Application::Q4()
{
	float speed = CalculateSpeedOfSoundInAir(30);

	std::cout << std::fixed;
	std::cout << std::setprecision(2);

	// time = distance/speed
	std::cout << " The speed of sound in air at 30C is " << speed << "m/s\n" << std::setprecision(8)
		<< " Time = distance/speed, so it will take the sound wave " << 35.f / speed << " Seconds\n To Reach the back of the auditorium" << std::endl;
}

void Application::Q5()
{
	float speed = CalculateSpeedOfSoundInAir(16);

	std::cout << std::fixed;
	std::cout << std::setprecision(2);

	std::cout << " The speed of sound in air at 16C is " << speed << "m/s\n" << std::setprecision(8)
		<< " distance = speed x time, so the thunder cloud is  " << speed * 3 << "m Away" << std::endl;
}

void Application::Q6()
{
	float speedSea = CalculateSpeedOfSoundInAir(20);
	float speedMtEve = CalculateSpeedOfSoundInAir(-32);

	std::cout << std::fixed;
	std::cout << std::setprecision(2);

	std::cout << " The speed of sound in air at sea level (20C) is " << speedSea << "m/s\n"
		<< " The speed of sound in air at the top of Mt.Everest(-32C) is " << speedMtEve << "m/s\n" << std::setprecision(8)
		<< " The difference in velocity is " << speedSea - speedMtEve << "m/s" << std::endl;
}

void Application::Q7()
{
	std::cout << "\n Assuming the frequancy of a second refers to to amount of time it takes to complete rotation of the clock "
		<< "\n It take 60 seconds for a complete rotation so 1/60 is " << 1.f / 60.f << "HZ\n" << std::endl;;
}

void Application::Q8()
{
	std::cout << "\n Assuming the frequancy of the earths rotation refers to the time it takes\n to spin around its own axis and not the sun\n"
		<< "\n It take 23 hours, 56 minutes, and 4.09 seconds for a complete rotation around its own axis\n"
		<< " This is 86164.09 seconds so 1/86164.09 is " << 1 / 86164.09 << "HZ\n" << std::endl;;
}

void Application::Q9()
{
	std::cout << "\n There are no waveforms to plot..... " << std::endl;
}

void Application::Q10()
{
	float speed = CalculateSpeedOfSoundInAir(21);

	std::cout << "\n Assuming a temprature of 21C, wavelength is speed/frequency\n"
		" The speed of sound in air at 21C is " << speed <<
		"\n So we have " << speed << "/1720 = " << CalculateWaveLength(1720, 21) << "m" << std::endl;
}

void Application::Q11()
{
	//If pipe on a pipe organ is built to have a wavelength of 4m�s, what is the frequency of the pipe ?
	float speed = CalculateSpeedOfSoundInAir(21);


	std::cout << "\n Assuming a temprature of 21C, frequency is speed/period\n"
		" The speed of sound in air at 21C is " << speed <<
		"\n So we have " << speed << "/4 = " << CalculateFrequency(4, 21) << "HZ" << std::endl;
}

void Application::Q12()
{
	std::cout << " SWL is calculated by 10 * log10(actual/referance)\n"
		" Where referance is 1 picowatt\n" << std::endl;

	std::cout << "\n 0.5 Watt: " << CalculateSWL(0.5f) << "dB\n" << std::endl;
	std::cout << "\n 1 Watt: " << CalculateSWL(1) << "dB" << std::endl;
	std::cout << "\n 2 Watt: " << CalculateSWL(0.25) << "dB" << std::endl;
	
}

void Application::Q13()
{
	std::cout << " SPL is calculated by 20 * log10(actual/referance)\n"
		" Where referance is 20 micronpascals\n" << std::endl;

	std::cout << " 1.5 PA: " << CalculateSPL(1.5) << "dB\n" << std::endl;
	//std::cout << "4 PA: " << CalculateSPL(4) << "dB" << std::endl;
	//std::cout << "1 PA: " << CalculateSPL(1) << "dB" << std::endl;
}

void Application::Q14()
{
	std::cout << " SIL is calculated by 10 * log10(actual/referance)\n"
		" Where actual is watts / 4*Pi(distance Squared)\n"
		" Referance Given in the question is 1 picowatt\n" << std::endl;

	std::cout << " 1 Metre: " << CalculateSIL(0.1f, 1, 1) << "dB" << std::endl;
	std::cout << " 2 Metre: " << CalculateSIL(0.1f, 2, 1) << "dB" << std::endl;
	std::cout << " 4 Metre: " << CalculateSIL(0.1f, 4, 1) << "dB\n" << std::endl;
}

void Application::Q15()
{
	std::cout << " This is the same as the previous question but we multiply \n"
		" Actual by the boundries\n" << std::endl;

	std::cout << " 1 boundries: " << CalculateSIL(0.1f, 2, 1) << "dB" << std::endl;
	std::cout << " 2 boundries: " << CalculateSIL(0.1f, 2, 2) << "dB" << std::endl;
	std::cout << " 3 boundries: " << CalculateSIL(0.1f, 2, 3) << "dB\n" << std::endl;
}
#pragma endregion

// Lab 2 Questions
#pragma region Lab 2 Questions

void Application::Q17()
{
	print("The Mean Free Path is 4V/S");

	float vol = CalculateVolume(2.f, 3.f, 5.f);
	float surfaceArea = CalculateSurfaceArea(2.f, 3.f, 5.f);

	std::cout << "Volume : " << vol << "m^3" << std::endl;
	std::cout << "Surface Area : " << surfaceArea << "m^2" << std::endl;

	float MFP = CalculateMeanFreePath(vol, surfaceArea);

	std::cout << "The Mean Free Path is  4 * " << vol << " / " << surfaceArea << std::endl;

	std::cout << "Mean Free Path: " << MFP << "m" << std::endl;

}

void Application::Q18()
{

	print("The Average Time Between Successive Reflections is is 4V/Sc");

	float vol = CalculateVolume(2.f, 3.f, 5.f);
	float surfaceArea = CalculateSurfaceArea(2.f, 3.f, 5.f);
	float speedOfSound = CalculateSpeedOfSoundInAir(21);

	std::cout << "Volume : " << vol << "m^3" << std::endl;
	std::cout << "Surface Area : " << surfaceArea << "m^2" << std::endl;
	std::cout << "Speed of sound at 21 degrees : " << speedOfSound << "mps" << std::endl;

	float MFT = CalculateTimeBetweenRefelections(vol, surfaceArea);

	std::cout << "The Mean Free Path is  4 * " << vol << " / " << surfaceArea << " * " << speedOfSound << std::endl;

	std::cout << "Mean Free Time: " << MFT << "s" << std::endl;
}

void Application::Q19()
{
#pragma region Room 1

	std::cout << " ROOM 1 - 250HZ " << std::endl;
	std::cout << " Walls: Brick (natural), Floor: Glazed Tile, Ceiling: Plasterboard " << std::endl;
	print("\n________________________________________________________________________________\n");

	float width = 4.f, length = 2.f, height = 2.f;
	float wallCO = BRICK_NATURAL[_250HZ];
	float floorCO = GLAZED_TILE[_250HZ];
	float ceilingCO = PLASTERBOARD[_250HZ];

	float rt60 = CalculateAbsorbtionForRoom(width, length, height, wallCO, floorCO, ceilingCO);

	std::cout << "\nRT60 For Room 1: " << rt60 << "s" << std::endl;
	print("\n________________________________________________________________________________\n");

#pragma endregion

#pragma region Room 2

	std::cout << " ROOM 2 - 250HZ " << std::endl;
	std::cout << " Walls: Brick (painted), Floor: Glazed Tile, Ceiling: Plasterboard " << std::endl;
	print("\n________________________________________________________________________________\n");

	//ONLY dif for rooom 2 is the walls are painted
	wallCO = BRICK_PAINTED[_250HZ];
	rt60 = CalculateAbsorbtionForRoom(width, length, height, wallCO, floorCO, ceilingCO);

	std::cout << "\nRT60 For Room 2: " << rt60 << "s" << std::endl;
	print("\n________________________________________________________________________________\n");

#pragma endregion

#pragma region Room 3
	std::cout << " ROOM 3 - 250HZ " << std::endl;
	std::cout << " Walls: Plasterboard, Floor: Wood on joists, Ceiling: Plaster" << std::endl;
	print("\n________________________________________________________________________________\n");

	width = 40.f, length = 30.f, height = 3.f;
	wallCO = PLASTERBOARD[_250HZ];
	floorCO = WOOD_FLOORING[_250HZ];
	ceilingCO = PLASTER[_250HZ];
	rt60 = CalculateAbsorbtionForRoom(width, length, height, wallCO, floorCO, ceilingCO);

	std::cout << "\nRT60 For Room 3: " << rt60 << "s" << std::endl;
	print("\n________________________________________________________________________________\n");

#pragma endregion

#pragma region Room 4

	std::cout << " ROOM 4 - 250HZ " << std::endl;
	std::cout << " Walls: Concrete block(coarse), Floor: Concrete (unpainted rough finish), Ceiling: Plaster" << std::endl;
	print("\n________________________________________________________________________________\n");

	width = 3.4f, length = 5.2f, height = 1.8f;
	wallCO =  CONCRETE_BLOCK[_250HZ];
	floorCO = CONCRETE[_250HZ];
	ceilingCO = PLASTER[_250HZ];
	rt60 = CalculateAbsorbtionForRoom(width, length, height, wallCO, floorCO, ceilingCO);

	std::cout << "\nRT60 For Room 4: " << rt60 << "s" << std::endl;
	print("\n________________________________________________________________________________\n");

#pragma endregion
}

float Application::CalculateAbsorbtionForRoom(float width, float length, float height, float wallCO, float floorCo, float ceilingCO)
{
	std::cout << "Width: " << width << "m, Length: " << length << "m, Height: " << height << "m" << std::endl;
	std::cout << "Total Volume: " << CalculateVolume(width, length, height) << "m^3" ;

	// Calculate Absorbtion for each Surface
	std::cout << "\nAbsorbtion for surface = area * Coefficient" << std::endl;

	std::cout << "\nWalls - ";
	float wallsAC = CalculateAbsorptionForSurface(2 * (width * height + length * height), wallCO);

	std::cout << "\nFloor - ";
	float floorAC = CalculateAbsorptionForSurface(width * length, floorCo);

	std::cout << "\nCeiling - ";
	float ceilingAC = CalculateAbsorptionForSurface(width * length, ceilingCO);

	std::cout << "\nTotal Absorbtion: " << wallsAC << " + " << floorAC << " + " << ceilingAC << std::endl;

	return CalculateRT60(CalculateVolume(width, length, height), wallsAC + floorAC + ceilingAC);
}

void Application::Q20()
{
	std::cout << " Walls: Brick (natural), Floor: Glazed Tile, Ceiling: Plasterboard " << std::endl;

	std::cout << " Picalo at - 250HZ " << std::endl;
	print("\n________________________________________________________________________________\n");

	float width = 30.f, length = 20.f, height = 10.f;
	float wallCO = CONCRETE_BLOCK[_250HZ];
	float floorCO = GLAZED_TILE[_250HZ];
	float ceilingCO = PLASTERBOARD[_250HZ];

	float rt60 = CalculateAbsorbtionForRoom(width, length, height, wallCO, floorCO, ceilingCO);

	std::cout << "\nRT60 For picalo at 250HZ " << rt60 << "s" << std::endl;
	print("\n________________________________________________________________________________\n");

	std::cout << " French horn at - 4KHZ " << std::endl;
	print("\n________________________________________________________________________________\n");

	wallCO = CONCRETE_BLOCK[_4KHZ];
	floorCO = GLAZED_TILE[_4KHZ];
	ceilingCO = PLASTERBOARD[_4KHZ];

	float rt60_2 = CalculateAbsorbtionForRoom(width, length, height, wallCO, floorCO, ceilingCO);

	std::cout << "\nRT60 For French Horn at 4KHZ " << rt60 << "s" << std::endl;
	print("\n________________________________________________________________________________\n");

	std::cout << "\nThe Difference in Reverberation Time is  " << rt60_2 << " - " << rt60 << std::endl;
	std::cout << "== " << rt60_2 - rt60 << "s " << std::endl;
}

void Application::Q21()
{
	CalculateFreqFlatPanel(0.1f, PLY_1_4_INCH);
}

void Application::Q22()
{
	CalculateFreqFlatPanel(0.2f, PLY_1_4_INCH);
}

void Application::Q23()
{
	CalculateFreqFlatPanel(0.2f, (PLY_1_2_INCH * 2));
}

void Application::Q24()
{
	std::cout << "\nAssuming a Plywood thickness of 1/2inch";
	std::cout << "\nAssuming a Surface area of 1m^2\n";

	float depth = pow(60.f / 170.f, 2) / PLY_1_2_INCH;

	std::cout << "Depth needed: " << depth << "m " << std::endl;

}

#pragma endregion
