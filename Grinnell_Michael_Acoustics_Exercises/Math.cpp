#include <cmath>

#include "Math.hpp"
#include "Constants.hpp"
#include <iostream>

#pragma region LAB 1 Formulas
float CalculatePeriod(float frequency)
{
	return 1.f / frequency;
}

float CalculateFrequency(float period, float temperature)
{
	return CalculateSpeedOfSoundInAir(temperature) / period;
}

float CalculateWaveLength(float frequancy, float temperature)
{
	return CalculateSpeedOfSoundInAir(temperature) / frequancy;
}

//Calculates the speed of sound in air (m/s) based on temprature(C)
//Forumla -> https://www.engineersedge.com/physics/speed_of_sound_13241.htm
float CalculateSpeedOfSoundInAir(float temperature)
{
	return ToMPS(643.855f * pow((ToKelvin(temperature) / 273.15f), 0.5f));
}

double YoungsModulus(double youngsMod, double density)
{
	return sqrt(youngsMod / density);
}

//Converts Degree C to Kelvin
float ToKelvin(float degreeC)
{
	return degreeC + 273.15;
}

//Converts Knots to metere/second
float ToMPS(float knots)
{
	return 0.5144444f * knots;
}

float CalculateSWL(float watts)
{
	return 10 * log10(watts / pow(10, -12));
}

float CalculateSPL(float RMS)
{
	return 20 * log10(RMS / (20 * pow(10, -6)));
}

float CalculateSIL(float watts, float distance, unsigned short boundaries)
{
	return 10 * log10(((boundaries * watts) / (4 * PI * pow(distance, 2))) / pow(10, -12));
}
#pragma endregion

#pragma region Lab 2 Formulas

float CalculateVolume(float width, float length, float height)
{
	return width * length * height;
}
//Assumes Cuboid
float CalculateSurfaceArea(float width, float length, float height)
{
	return  (length * width + width * height + length * height) * 2;
}

float CalculateArea(float width, float length)
{
	return width * length;
}

float CalculateMeanFreePath(float volume, float surfaceArea)
{
	return 4 * volume / surfaceArea;
}

float CalculateTimeBetweenRefelections(float volume, float surfaceArea)
{
	return (4 * volume) / (surfaceArea * CalculateSpeedOfSoundInAir(21));
}

float CalculateAbsorptionForSurface(float area, float ac)
{
	std::cout << " Area: " << area << "m^2,  AC: " << ac << std::endl;
	std::cout << area << " * " << ac << " = " << area * ac << std::endl;

	return area * ac;
}

float CalculateRT60(float volume, float absorbtion)
{
	std::cout << "\n\nRT60: 0.161 / " << absorbtion << " = " << 0.161 / absorbtion << std::endl;

	return (0.161 * volume) / absorbtion;
}

//Calculates frequency of a flat panel absorber
float CalculateFreqFlatPanel(float cavityDepth, float density)
{
	float sum = cavityDepth * density;
	float sqrt = std::sqrt(sum);
	float freq = 60 / sqrt;

	std::cout << cavityDepth << "m * " << density << "kg/m^2 = " << sum << std::endl;
	std::cout << "Square root of : " << sum << " = " << sqrt << std::endl;
	std::cout << "Frequency : 60 / " <<  sqrt <<  " = " << freq << "HZ" << std::endl;

	return freq;
}

#pragma endregion
