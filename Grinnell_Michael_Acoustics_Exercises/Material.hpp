#pragma once
#include <iostream>

class Material
{
public:
	Material(std::string name, double youngsMod, unsigned int density);

	std::string GetName();
	double GetYoungsMod();
	unsigned int GetDensity();
private:
	std::string mName;
	double mYoungsMod;
	unsigned int mDensity;

};