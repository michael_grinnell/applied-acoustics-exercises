#include "Materials.hpp"
#include "Constants.hpp"
#include "Math.hpp"

#include <iomanip>

Materials::Materials()
{
	AddMaterials();
}

void Materials::AddMaterials()
{
	mMaterialList.push_back(Material("Steel", Y_MOD_STEEL, DENSITY_STEEL));
	mMaterialList.push_back(Material("Aluminium", Y_MOD_ALUMINIUM, DENSITY_ALUMINIUM));
	mMaterialList.push_back(Material("Lead", Y_MOD_LEAD, DENSITY_LEAD));
	mMaterialList.push_back(Material("Glass", Y_MOD_GLASS, DENSITY_GLASS));
	mMaterialList.push_back(Material("Concrete", Y_MOD_CONCRETE, DENSITY_CONCRETE));
	mMaterialList.push_back(Material("Fresh Water", Y_MOD_WATER, DENSITY_WATER));
	mMaterialList.push_back(Material("Beech wood (along the grain) ", Y_MOD_BEACH_WOOD_X, DENSITY_BEACH_WOOD));
	mMaterialList.push_back(Material("Beech wood (across the grain)", Y_MOD_BEACH_WOOD_Y, DENSITY_BEACH_WOOD));
}

void Materials::Print()
{
	std::cout << std::fixed;
	std::cout << std::setprecision(8);

	for (Material& material : mMaterialList)
		std::cout << "\n The Speed of Sound in " << material.GetName() << " is " << YoungsModulus(material.GetYoungsMod(), material.GetDensity()) << "m/s" << std::endl;
}
