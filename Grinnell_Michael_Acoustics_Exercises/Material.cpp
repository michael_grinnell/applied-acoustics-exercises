#include "Material.hpp"

Material::Material(std::string name, double youngsMod, unsigned int density) :
	mName(name),
	mYoungsMod(youngsMod),
	mDensity(density)
{

}

std::string Material::GetName()
{
	return mName;
}

double Material::GetYoungsMod()
{
	return mYoungsMod;
}

unsigned int Material::GetDensity()
{
	return mDensity;
}

