#pragma once

float CalculatePeriod(float frequency);
float CalculateFrequency(float period, float temperature);
float CalculateWaveLength(float frequancy, float temperature);
float CalculateSpeedOfSoundInAir(float temperature);
double YoungsModulus(double youngsMod, double density);

float ToKelvin(float degreeC);
float ToMPS(float knots);

float CalculateSWL(float watts);
float CalculateSPL(float RMS);
float CalculateSIL(float watts, float distance, unsigned short boundaries);

float CalculateVolume(float width, float length, float height);
float CalculateSurfaceArea(float width, float length, float height);
float CalculateArea(float width, float length);

float CalculateMeanFreePath(float volume, float surfaceArea);
float CalculateTimeBetweenRefelections(float volume, float surfaceArea);
float CalculateAbsorptionForSurface(float area, float ac);

float CalculateRT60(float volume, float absorbtion);

float CalculateFreqFlatPanel(float cavityDepth, float density);