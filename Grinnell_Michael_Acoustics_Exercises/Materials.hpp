#pragma once

#include <vector>
#include "Material.hpp"


class Materials
{
public:
	Materials();

private:
	void AddMaterials();

public:
	void Print();

private:
	std::vector<Material> mMaterialList;
};