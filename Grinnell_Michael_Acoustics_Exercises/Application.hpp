#pragma once

class Application
{
public:
	Application();
	void start();

private:
	void run();

private:
	//Lab 1
	void Q1();
	void Q2();
	void Q3();
	void Q4();
	void Q5();
	void Q6();
	void Q7();
	void Q8();
	void Q9();
	void Q10();
	void Q11();
	void Q12();
	void Q13();
	void Q14();
	void Q15();

	//Lab 2
	void Q17();
	void Q18();
	void Q19();
	void Q20();
	void Q21();
	void Q22();
	void Q23();
	void Q24();

	float CalculateAbsorbtionForRoom(float width, float length, float height, float wallCO, float floorCo, float ceilingCO);
};